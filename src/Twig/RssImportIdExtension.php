<?php
declare(strict_types=1);

namespace RapidData\ContaoRapidUtilitiesBundle\Twig;

use Twig\Extension\AbstractExtension;
use Contao\NewsArchiveModel;
use Twig\TwigFunction;

class RssImportIdExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('isArtikelportalArticle', [$this, 'isArtikelportalArticle'])
        ];
    }

    /**
     * @param int $input
     * @return bool
     */

    public function isArtikelportalArticle(int $input):bool
    {
        $objArchive = NewsArchiveModel::findById($input);
        // @phpstan-ignore-next-line
        if ($objArchive->rssimp_published === '1') {
            return true;
        } else {
            return false;
        }
    }
}