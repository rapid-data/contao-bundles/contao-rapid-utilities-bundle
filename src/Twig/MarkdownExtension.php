<?php
declare(strict_types=1);

namespace RapidData\ContaoRapidUtilitiesBundle\Twig;

use Michelf\Markdown;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class MarkdownExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('markdown', [$this, 'markdown2Html'],  ['is_safe' => ['html']])
        ];
    }

    /**
     * Convert markdown to HTML and remove the enclosing p-Tag using Michelf/Markdown
     * @param string $input The markdown string to convert
     * @return string The resulting HTML string
     */
    public function markdown2Html(string $input): string
    {
        $md = new Markdown();
        $md->enhanced_ordered_list = true;
        return preg_replace('%<p(.*?)>|</p>%s','',$md->transform($input));
    }
}
