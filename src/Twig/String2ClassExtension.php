<?php
declare(strict_types=1);

namespace RapidData\ContaoRapidUtilitiesBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class String2ClassExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('str2cls', [$this, 'string2Class'])
        ];
    }

    /**
     * Convert strings to a class like string
     * @param string $input The string to convert
     * @return string The resulting class name
     */
    public function string2Class(string $input): string
    {
        $input = strtolower($input);
        $input = preg_replace('/ä/', 'ae', $input);
        $input = preg_replace('/ö/', 'oe', $input);
        $input = preg_replace('/ü/', 'ue', $input);
        $input = preg_replace('/ß/', 'ss', $input);
        $input = preg_replace('/ /', '-', $input);
        $input = preg_replace('/\./', '', $input);
        $input = preg_replace('/,/', '', $input);
        $input = preg_replace('/\(/', '', $input);
        $input = preg_replace('/\)/', '', $input);
        return $input;
    }
}
