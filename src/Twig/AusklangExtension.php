<?php
declare(strict_types=1);

namespace RapidData\ContaoRapidUtilitiesBundle\Twig;

use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AusklangExtension extends AbstractExtension
{
    private HttpClientInterface $httpClient;
    private LoggerInterface $logger;

    public function __construct(HttpClientInterface $httpClient, LoggerInterface $logger)
    {
        $this->httpClient = $httpClient;
        $this->logger = $logger;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('getAusklangInfo', [$this, 'getAusklangInfo'])
        ];
    }

    public function getAusklangInfo(): array
    {
        $customerId = (int)$GLOBALS['TL_CONFIG']['bpCustomerId'];

        if ($customerId) {
            $this->logger->info("[TwigAusklangExtension] Customer id is $customerId");

            try {
                $apiResponse = $this->httpClient->request(
                    'GET',
                    "https://api.bestatterwebtool.de/ausklang/subscription/$customerId"
                );

                $responseStatus = $apiResponse->getStatusCode();
                if ($responseStatus === 200) {
                    return $apiResponse->toArray();
                }
                $this->logger->warning("[TwigAusklangExtension] Abnormal API-Response status $responseStatus!");
            } catch (TransportExceptionInterface $e) {
                $this->logger->error(
                    "[TwigAusklangExtension] Ausklang-Request failed for customerId $customerId with " . $e->getMessage(),
                    $e->getTrace()
                );
            } catch (Exception $e) {
                $this->logger->error(
                    "[TwigAusklangExtension] Something went wrong for customerId $customerId with " . $e->getMessage(),
                    $e->getTrace()
                );
            }
        }
        return ['hasSubscription' => false];
    }
}
