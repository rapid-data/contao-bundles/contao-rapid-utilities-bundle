<?php
declare(strict_types=1);

namespace RapidData\ContaoRapidUtilitiesBundle\Twig;

use Contao\Controller;
use Contao\CoreBundle\Framework\ContaoFramework;
use Contao\StringUtil;
use RapidData\ContaoRapidUtilitiesBundle\Util\PhoneNumberUtil;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class StringExtension extends AbstractExtension
{
    private $numberFormat;
    /**
     * @var ContaoFramework
     */
    private $contaoFramework;

    public function __construct(ParameterBagInterface $parameterBag, ContaoFramework $contaoFramework)
    {
        $this->numberFormat = $parameterBag->has('phone_format')
            ? $parameterBag->get('phone_format')
            : '0VVVVV ##########';
        $this->contaoFramework = $contaoFramework;
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('email', [StringUtil::class, 'encodeEmail'],  ['is_safe' => ['html']]),
            new TwigFilter('format_phone', [$this, 'formatPhoneNumber']),
            new TwigFilter('link_url', [$this, 'linkUrl'],  ['is_safe' => ['html']])
        ];
    }

    public function formatPhoneNumber(string $intlNumber, ?string $format = null): string
    {
        if (!$format) {
            $format = $this->numberFormat;
        }
        return PhoneNumberUtil::format($intlNumber, $format);
    }

    public function linkUrl(string $input, bool $cacheResult = true): string
    {
        if (!$this->contaoFramework->isInitialized()) {
            $this->contaoFramework->initialize();
        }

        return $this->contaoFramework->getAdapter(Controller::class)->replaceInsertTags("{{link_url::$input}}", $cacheResult);
    }
}
