<?php
declare(strict_types=1);

namespace RapidData\ContaoRapidUtilitiesBundle\Twig;

use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class NewsInteropExtension extends AbstractExtension
{
    /**
     * @var ?Request
     */
    private $masterRequest;

    public function __construct(RequestStack $requestStack)
    {
        $this->masterRequest = $requestStack->getMasterRequest();
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('getClosureResult', [$this, 'executeClosure'])
        ];
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('isInUrl', [$this, 'isInUrl'])
        ];
    }

    public function executeClosure(\Closure $closure, $arguments = [])
    {
        return $closure(...$arguments);
    }

    public function isInUrl(string $target): bool
    {
        if (!$this->masterRequest) {
            return false;
        }

        $path = urldecode($this->masterRequest->getPathInfo());

        return Str::contains($path, $target);
    }
}
