<?php

use RapidData\ContaoRapidUtilitiesBundle\Util\BackendUtil;

$GLOBALS['TL_DCA']['tl_module']['fields']['navbarModules'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_module']['navbarModules'],
    'exclude'   => true,
    'inputType' => 'multiColumnWizard',
    'eval'      => [
        'tl_class'     => 'clr',
        'columnFields' =>
            [
                'module'   => [
                    'label'            => $GLOBALS['TL_LANG']['tl_module']['module'],
                    'inputType'        => 'select',
                    'options_callback' => [BackendUtil::class, 'getModules'],
                    'eval'             => [
                        'style'              => 'width: 250px',
                        'includeBlankOption' => true,
                        'chosen'             => true,
                    ],
                ],
                'inactive' => [
                    'label'     => &$GLOBALS['TL_LANG']['tl_module']['inactive'],
                    'inputType' => 'checkbox',
                    'eval'      => [],
                ],
            ],
    ],
    'sql'       => 'blob NULL',
];


$GLOBALS['TL_DCA']['tl_module']['fields']['toggleBreakpoint'] = [
    'exclude'   => true,
    'inputType'        => 'select',
    'options' => [
        'sm',
        'md',
        'lg',
        'xl',
        'xxl',
        'fhd'
    ],
    'eval'      => [
        'tl_class'     => 'w50',
        'includeBlankOption' => true,
        'chosen'             => true
    ],
    'sql'       => "varchar(64) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_module']['fields']['targetModuleId'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_module']['targetModuleId'],
    'exclude'   => true,
    'inputType'        => 'select',
    'options_callback' => [BackendUtil::class, 'getModules'],
    'eval'      => [
        'tl_class'     => 'clr w50',
        'includeBlankOption' => false,
        'chosen'             => true
    ],
    'sql'       => 'int default 0',
];

$GLOBALS['TL_DCA']['tl_module']['palettes']['navbar'] = '{title_legend},name,type;{config_legend},navbarModules,toggleBreakpoint;{template_legend},customTpl';
$GLOBALS['TL_DCA']['tl_module']['palettes']['navbar_toggler'] = '{title_legend},name,type;{config_legend},targetModuleId;{template_legend},customTpl';