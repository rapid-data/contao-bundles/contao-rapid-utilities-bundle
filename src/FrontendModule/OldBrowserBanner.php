<?php
declare(strict_types=1);

namespace RapidData\ContaoRapidUtilitiesBundle\FrontendModule;

use Contao\CoreBundle\Controller\FrontendModule\AbstractFrontendModuleController;
use Contao\CoreBundle\ServiceAnnotation\FrontendModule;
use Contao\Environment;
use Contao\ModuleModel;
use Contao\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Fragment for displaying a warning when a user visits the site with Internet Explorer
 *
 * @FrontendModule(category="miscellaneous", template="mod_old-browser-banner", renderer="esi")
 */
class OldBrowserBanner extends AbstractFrontendModuleController
{
    protected function getResponse(Template $template, ModuleModel $model, Request $request): ?Response
    {
        $ua = Environment::get('agent');
        $isIE = $ua->browser === 'ie';

        $request->headers->set('Cache-Control', 'private');
        $request->headers->addCacheControlDirective('private');
        $request->headers->addCacheControlDirective('maxage', '120');

        $template->setData(['isIE' => $isIE]);
        return $template->getResponse();
    }
}
