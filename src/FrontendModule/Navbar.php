<?php
declare(strict_types=1);

namespace RapidData\ContaoRapidUtilitiesBundle\FrontendModule;

use Contao\CoreBundle\Controller\FrontendModule\AbstractFrontendModuleController;
use Contao\CoreBundle\ServiceAnnotation\FrontendModule;
use Contao\Module;
use Contao\ModuleModel;
use Contao\StringUtil;
use Contao\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Navbar
 * @package RapidData\ContaoRapidUtilitiesBundle\FrontendModule
 * @FrontendModule(category="navigation", template="mod_navbar")
 */

class Navbar extends AbstractFrontendModuleController
{
    protected function getResponse(Template $template, ModuleModel $model, Request $request): ?Response
    {
        // @phpstan-ignore-next-line
        $modules = collect($model->navbarModules ? StringUtil::deserialize($model->navbarModules): [])
            ->filter(function(array $navModule) {
                return !$navModule['inactive'];
            })
            ->map(function (array $navModule) {
                return Module::getFrontendModule($navModule['module']);
            });

        $template->setData([
            'modules' => $modules,
            // @phpstan-ignore-next-line
            'toggleBreakpoint' => 'navbar-expand-'.$model->toggleBreakpoint,
            'id' => 'navbar-'.$model->id
        ]);
        return $template->getResponse();
    }
}
