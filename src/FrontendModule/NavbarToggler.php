<?php
declare(strict_types=1);

namespace RapidData\ContaoRapidUtilitiesBundle\FrontendModule;

use Contao\CoreBundle\Controller\FrontendModule\AbstractFrontendModuleController;
use Contao\CoreBundle\ServiceAnnotation\FrontendModule;
use Contao\ModuleModel;
use Contao\StringUtil;
use Contao\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class NavbarToggler
 * @package RapidData\ContaoRapidUtilitiesBundle\FrontendModule
 * @FrontendModule(category="navigation", template="mod_navbar-toggler")
 */
class NavbarToggler extends AbstractFrontendModuleController
{

    protected function getResponse(Template $template, ModuleModel $model, Request $request): ?Response
    {
        // @phpstan-ignore-next-line
        $targetModuleModel = ModuleModel::findById($model->targetModuleId);

        if (!$targetModuleModel) {
            return new Response();
        }
        [$id] = StringUtil::deserialize($targetModuleModel->cssID);

        $template->setData([
            'targetId' => $id ?? ''
        ]);
        return $template->getResponse();
    }
}
