<?php
namespace RapidData\ContaoRapidUtilitiesBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\StringUtil;
use Psr\Log\LoggerInterface;
use RapidData\ContaoRapidUtilitiesBundle\Util\PhoneNumberUtil;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * @Hook("replaceInsertTags")
 */
class ReplaceInsertTagsListener
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    private $phoneFormat;

    public function __construct(LoggerInterface $logger, ParameterBagInterface $parameterBag)
    {
        $this->logger = $logger;
        if ($parameterBag->has('phone_format')) {
            $this->phoneFormat = $parameterBag->get('phone_format');
        }
    }

    public function __invoke(
        string $insertTag,
        bool $useCache,
        string $cachedValue,
        array $flags,
        array $tags,
        array $cache,
        int $_rit,
        int $_cnt
    )
    {
        [$tag, $argument] = explode('::', $insertTag);

        if ('format_phone' === $tag) {
            [$phoneNumber, $format] = explode('>>',StringUtil::decodeEntities($argument));

            // check if phone number starts with a +, so the chance is high it is an international format
            if (!str_starts_with($phoneNumber, '+')) {
                $this->logger->error("Phone number $phoneNumber has the wrong format! It needs to be international +XX (XXXX) XXXXXX");
                return false;
            }

            // check if the format argument was provided
            if (!$format) {
                if (!$this->phoneFormat) {
                    $this->logger->error("format_phone: No format provided! Please provide a format! Example: {{format_phone::+49 (1234) 22445>>(VVVV) ## ## #}}");
                    // return nonformatted number otherwise
                    return $phoneNumber;
                }
                return PhoneNumberUtil::format($phoneNumber, $this->phoneFormat);
            }

            // if all arguments are available format the number and return it.
            return PhoneNumberUtil::format($phoneNumber, $format);
        }

        return false;
    }
}
