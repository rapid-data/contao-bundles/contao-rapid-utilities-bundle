<?php

namespace RapidData\ContaoRapidUtilitiesBundle\Util;

use Contao\Controller;
use Contao\StringUtil;

class PhoneNumberUtil
{
    /**
     * Formats an international source phone number using a pattern, replacing insert tags along the way.
     * @example
     * $formattedNumber = PhoneNumberUtil::format("+49 (451) 6999549", "(0VVVV) ## ## ##-#");
     * echo $formattedNumber; // (0451) 69 99 54-9
     * @param string $intlPhoneNumber
     * @param string $format
     * @return string
     */
    public static function format(string $intlPhoneNumber, string $format): string
    {
        $phoneNumber = Controller::replaceInsertTags($intlPhoneNumber);
        $phoneFormat = Controller::replaceInsertTags($format);
        $formattedNumber = str_split(StringUtil::decodeEntities($phoneFormat));
        // split the phone number by space
        $numberStacks = explode(' ', StringUtil::decodeEntities($phoneNumber));
        // remove any eventual empty entries resulting from double spaces
        // @phpstan-ignore-next-line
        $numberStacks = array_values(array_filter($numberStacks, 'strlen'));

        // remove international num prefix
        $numberStacks[0] = str_replace('+', '', $numberStacks[0]);
        // remove braces around regional code
        $numberStacks[1] = str_replace('(', '', $numberStacks[1]);
        $numberStacks[1] = str_replace(')', '', $numberStacks[1]);

        // split number strings into arrays
        $intlStack = str_split($numberStacks[0]);
        $regStack = str_split($numberStacks[1]);
        $lineStack = str_split($numberStacks[2]);

        // parse every character in the format template and
        // replace the known characters
        foreach ($formattedNumber as $idx => $character) {
            // replace character 'I' as long as there are
            // items on the international stack.
            if ($character == 'I') {
                if (count($intlStack) > 0) {
                    $formattedNumber[$idx] = array_shift($intlStack);
                } else {
                    $formattedNumber[$idx] = '';
                }
            } elseif ($character == 'V') {
                // replace character 'V' as long as there are
                // items on the regional code stack.
                if (count($regStack) > 0) {
                    $formattedNumber[$idx] = array_shift($regStack);
                } else {
                    $formattedNumber[$idx] = '';
                }
            } elseif ($character == '#') {
                // replace character '#' as long as there are
                // items on the line number stack.
                if (count($lineStack) > 0) {
                    $formattedNumber[$idx] = array_shift($lineStack);
                } else {
                    $formattedNumber[$idx] = '';
                }
            }
        }
        $result = implode($formattedNumber);

        // strip &nbsp; at the end if it exists
        if (str_ends_with($result, "\xc2\xa0")) {
            $result = substr($result, 0, strlen($result) - 2);
        }

        return trim($result);
    }
}
