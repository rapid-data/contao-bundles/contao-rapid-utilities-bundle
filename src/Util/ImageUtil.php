<?php
declare(strict_types=1);

namespace RapidData\ContaoRapidUtilitiesBundle\Util;

use Contao\FilesModel;
use Contao\StringUtil;
use Contao\System;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

/**
 * Class ImageUtil
 *
 * A helper class for creating images and pictures easily
 * without having to write the since Contao 4 overly complicated code
 * required to get image data.
 *
 * @package RapidData\ContaoRapidUtilitiesBundle\Util
 */
class ImageUtil
{
    /**
     * Creates a picture object from the given FilesModel with the given ImageSize
     * and incorporates it into the template data under 'picture'.
     *
     * @param FilesModel $fileModel
     * @param int $sizeId
     * @return array
     * @throws InvalidArgumentException
     * @throws ServiceCircularReferenceException
     * @throws ServiceNotFoundException
     */
    public static function createPictureWithMeta(FilesModel $fileModel, int $sizeId): array
    {
        $container = System::getContainer();
        $rootDir = $container->getParameter('kernel.project_dir');

        $picture = $container->get('contao.image.picture_factory')
            ->create($rootDir . '/' . $fileModel->path, $sizeId);

        $pictureArray = [
            'img' => $picture->getImg($rootDir),
            'sources' => $picture->getSources($rootDir),
        ];

        $metaForFileModel = MetaUtil::getMetaForFileModel($fileModel);
        $pictureArray = array_merge($pictureArray, $metaForFileModel);
        return $pictureArray;
    }

    /**
     * Creates a picture object from a binary UUID
     *
     * @param mixed $binaryUuid
     * @param mixed $serializedSize
     * @return array
     * @throws InvalidArgumentException
     * @throws ServiceCircularReferenceException
     * @throws ServiceNotFoundException
     */
    public static function createPictureFromBinaryUuid($binaryUuid, $serializedSize): array
    {
        $uuid = StringUtil::binToUuid($binaryUuid);
        $fileModel = FilesModel::findByUuid($uuid);
        $imageSizeId = (int)StringUtil::deserialize($serializedSize)[2];

        if ($fileModel !== null) {
            return self::createPictureWithMeta($fileModel, $imageSizeId);
        }

        return [];
    }

    /**
     * Creates an URL for the resized version of an image.
     *
     * @param string $uuid The UUID of the source image
     * @param string|null $imageSizeId The image size id
     * @return string|null The URL to the resized image or null if the UUID was invalid.
     */
    public static function createResizedImageUrl(string $uuid, ?string $imageSizeId): ?string
    {
        $container = System::getContainer();
        $rootDir = $container->getParameter('kernel.project_dir');

        $fileModel = FilesModel::findByUuid($uuid);

        if ($fileModel === null) {
            return null;
        }


        $image = $container->get('contao.image.image_factory')
            ->create($rootDir . '/' . $fileModel->path, $imageSizeId);

        return $image->getUrl($rootDir);
    }
}
