<?php
declare(strict_types=1);

namespace RapidData\ContaoRapidUtilitiesBundle\Util;

use Contao\FilesModel;
use Contao\Frontend;

class MetaUtil
{
    /**
     * @param FilesModel $fileModel
     * @return array
     */
    public static function getMetaForFileModel(FilesModel $fileModel): array
    {
        // @phpstan-ignore-next-line
        if (TL_MODE === 'FE') {
            global $objPage;

            $arrMeta = Frontend::getMetaData($fileModel->meta, $objPage->language);

            if (empty($arrMeta) && $objPage->rootFallbackLanguage !== null) {
                $arrMeta = Frontend::getMetaData($fileModel->meta, $objPage->rootFallbackLanguage);
            }
        } else {
            $arrMeta = Frontend::getMetaData($fileModel->meta, $GLOBALS['TL_LANGUAGE']);
        }
        return $arrMeta;
    }
}
