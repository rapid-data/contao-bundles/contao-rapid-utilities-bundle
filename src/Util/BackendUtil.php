<?php
declare(strict_types=1);

namespace RapidData\ContaoRapidUtilitiesBundle\Util;

use Contao\Input;
use Contao\ModuleModel;

class BackendUtil
{
    public static function getModules(): array
    {
        $self = Input::get('id') ?? 0;
        return collect(ModuleModel::findAll()->getModels())
            ->filter(function (ModuleModel $moduleModel) use ($self) {
                return $moduleModel->id !== $self;
            })
            ->mapWithKeys(function (ModuleModel $moduleModel) {
                return [$moduleModel->id => "$moduleModel->name [$moduleModel->type]"];
            })
            ->toArray();
    }
}
