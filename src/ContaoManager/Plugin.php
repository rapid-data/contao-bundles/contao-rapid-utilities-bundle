<?php
declare(strict_types=1);

namespace RapidData\ContaoRapidUtilitiesBundle\ContaoManager;

use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\CoreBundle\ContaoCoreBundle;
use RapidData\ContaoRapidUtilitiesBundle\ContaoRapidUtilitiesBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser): array
    {
        return [
            BundleConfig::create(ContaoRapidUtilitiesBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class]),
        ];
    }
}
