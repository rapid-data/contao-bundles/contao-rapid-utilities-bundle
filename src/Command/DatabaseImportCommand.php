<?php
declare(strict_types=1);

namespace RapidData\ContaoRapidUtilitiesBundle\Command;

use Doctrine\DBAL\Connection;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DatabaseImportCommand extends Command
{
    /**
     * @var string $database
     */
    private $database;
    /**
     * @var string $username
     */
    private $username;
    /**
     * @var string $password
     */
    private $password;
    /**
     * @var string $host
     */
    private $host;
    /**
     * @var int $port
     */
    private $port;
    /**
     * @var string $path
     */
    private $path;

    public function __construct(Connection $connection)
    {
        $params = $connection->getParams();
        $this->host = $params['host'];
        $this->port = (int)$params['port'];
        $this->username = $params['user'];
        $this->password = $params['password'];
        $this->database = $params['dbname'];

        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName('rapid:database:import')
            ->setDescription('Imports a SQL dump from file into the configured Database. WARNING: All existing data is deleted!')
            ->addArgument('file', InputArgument::REQUIRED, 'Absolute path for the file you want imported.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        $this->path = $input->getArgument('file') ? (string)$input->getArgument('file') : 'dump.sql' ;

        $output->writeln(sprintf('<comment>Importing <fg=green>%s</fg=green> into <fg=green>%s</fg=green> </comment>', $this->path, $this->database ));

        $this->importDatabase();

        $output->writeln('<comment>All done.</comment>');

        return 0;
    }

    /**
     * Runs the mysql commandline program to import the database
     *
     * @throws Exception
     */
    private function importDatabase(): void
    {
        $cmd = $this->host === 'localhost'
            ? sprintf('mysql %s -u %s --password=%s < %s',
                $this->database,
                $this->username,
                $this->password,
                $this->path)
            : sprintf('mysql %s -h %s -P %u -u %s --password=%s < %s',
                $this->database,
                $this->host,
                $this->port,
                $this->username,
                $this->password,
                $this->path);

        $result = $this->runCommand($cmd);

        if($result['exit_status'] > 0) {
            throw new Exception('Could not import database: ' . var_export($result['output'], true));
        }
    }

    /**
     * Runs a system command, returns the output, what more do you NEED?
     *
     * @param mixed $command
     * @return array
     */
    protected function runCommand($command): array
    {
        $command .=" >&1";
        exec($command, $output, $exit_status);
        return array(
            "output"      => $output
        , "exit_status" => $exit_status
        );
    }
}
