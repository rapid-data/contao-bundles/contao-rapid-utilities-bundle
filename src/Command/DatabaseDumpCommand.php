<?php
declare(strict_types=1);

namespace RapidData\ContaoRapidUtilitiesBundle\Command;

use Doctrine\DBAL\Connection;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class DatabaseDumpCommand extends Command
{
    /**
     * @var string $database
     */
    private $database;
    /**
     * @var string $username
     */
    private $username;
    /**
     * @var string $password
     */
    private $password;
    /**
     * @var string $host
     */
    private $host;
    /**
     * @var int $port
     */
    private $port;
    /**
     * @var string $path
     */
    private $path;

    /** filesystem utility
     * @var Filesystem
     */
    private $fs;

    public function __construct(Connection $connection)
    {
        $params = $connection->getParams();
        $this->host = $params['host'];
        $this->port = (int)$params['port'];
        $this->username = $params['user'];
        $this->password = $params['password'];
        $this->database = $params['dbname'];

        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName('rapid:database:dump')
            ->setDescription('Dump database.')
            ->addArgument('file', InputArgument::REQUIRED, 'Absolute path for the file you need to dump database to.');
    }

    /**
     * Executes the command
     *
     * @param InputInterface $input The input interface providing the input data
     * @param OutputInterface $output
     * @return int|null
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        $this->path = $input->getArgument('file') ? (string)$input->getArgument('file') : 'dump.sql' ;
        $this->fs = new Filesystem() ;

        $output->writeln(sprintf('<comment>Dumping <fg=green>%s</fg=green> to <fg=green>%s</fg=green> </comment>', $this->database, $this->path ));

        $this->createDirectoryIfRequired();

        $this->dumpDatabase();

        $output->writeln('<comment>All done.</comment>');

        return 0;
    }

    private function createDirectoryIfRequired(): void
    {
        if (! $this->fs->exists($this->path)){
            $this->fs->mkdir(dirname($this->path));
        }
    }

    /**
     * Executes mysqldump on the command line
     *
     * @throws Exception
     */
    private function dumpDatabase(): void
    {
        $cmd = $this->host === 'localhost'
            ? sprintf('mysqldump -B %s -u %s --password=%s --no-create-db=TRUE --tables > %s',
                $this->database,
                $this->username,
                $this->password,
                $this->path)
            : sprintf('mysqldump -B %s -h %s -P %u -u %s --password=%s --no-create-db=TRUE --tables > %s',
                $this->database,
                $this->host,
                $this->port,
                $this->username,
                $this->password,
                $this->path);

        $result = $this->runCommand($cmd);

        if($result['exit_status'] > 0) {
            throw new Exception('Could not dump database: ' . var_export($result['output'], true));
        }
    }

    /**
     * Runs a system command, returns the output, what more do you NEED?
     *
     * @param mixed $command
     * @return array
     */
    protected function runCommand($command): array
    {
        $command .=" >&1";
        exec($command, $output, $exit_status);
        return array(
            "output"      => $output
        , "exit_status" => $exit_status
        );
    }
}
