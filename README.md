<div align="center">
    <h1>Rapid Data Utilities Bundle<br>
    <small>für das Contao 4 CMS</small></h1>
</div>

<strong>
    Dieses Bundle enthält generelle Werkzeuge und Dinge, welche bei nahezu allen neuen (ggf. alten) Contao 4 Seiten gebraucht werden können, aber kein eigenes Bundle rechtfertigen.
</strong>

## Insert-Tags
Das Bundle stellt folgende Insert-Tags zur Verfügung:


| Insert-Tag | Parameter | Beschreibung |
| ---------- | --------- | ------------ |
| `{{format_phone::[TelefonNummer]>>[Format]}}`| `[TelefonNummer]` => Nummer im Internationalen Format +49 (0000) 999999 <br> `[Format]` => Zielformat, z.B. 0VVVVV ### ###-## | Formatiert eine Telefonnummer nach einem gewünschten Schema. Das Format kann auch weggelassen werden und stattdessen in der `config.yml` unter `parameters: ` als <br> `phone_format: "0VVVVV ## ## ###"` hinterlegt werden. |


## Telefonnummer-Formatierung
Das Bundle bringt ein Utility zur Formatierung von Telefonnummern mit.<br>
Mit der Klasse `RapidData\ContaoRapidUtilitiesBundle\Util\PhoneNumberUtil` kann eine Telefonnummer im internationalen Format in eine beliebige Formatierung gebracht werden:
```php
use RapidData\ContaoRapidUtilitiesBundle\Util\PhoneNumberUtil;

$formattedNumber = PhoneNumberUtil::format("+49 (451) 6999549", "(0VVVV) ## ## ##-#");
```
Der im Abschnitt __Inser-Tags__ aufgelistete Insert-Tag erfüllt ebenfalls diese Funktion.

## Contao Bild-Template generienen
Im Bundle ist ebenfalls das `RapidData\ContaoRapidUtilitiesBundle\Util\ImageUtil` und `RapidData\ContaoRapidUtilitiesBundle\Util\MetaUtil` enthalten.
Das `ImageUtil` erleichtert zum Beispiel die Generierung der für das `picture_default.html5` erforderlichen Daten.<br>
Es kann direkt mit der binären UUID aus einem Contao-FilePicker und einer gewählten Größe aus der Größenauswahl gefüttert werden:
```php
use RapidData\ContaoRapidUtilitiesBundle\Util\ImageUtil;

$picture = ImageUtil::createPictureFromBinaryUuid($this->imageSRC, $this->imageSize);
$this->insert('picture_default', $picture);
```

Das Meta-Util dagegen sucht für ein bestimmtes FilesModel die Meta-Daten aus der Dateiverwaltung heraus.

## Twig Filter
Das Bundle bringt ebenfalls Twig-Filter mit:

| Filter | Beschreibung |
| ------ | ------------ |
| `email` | Equivalent des Contao-Insert-Tag {{email_url::*}} |
| `format_phone` | Nutzt das oben beschriebene PhoneNumberUtil zur Formatierung von Telefonnummern |
| `link_url` | Equivalent zum Contao-Insert-Tag {{link_url::*}} |

## Old-Browser-Banner
Das Bundle stellt ebenfalls das Modul __Browser-Check__ zur Verfügung.<br>
Dieses kann im Seitenlayout eingebunden werden und zeigt beim Aufruf der Seite im Internet-Explorer einen Warnhinweis an.

## Datenbank-Kommandos
Weiterhin sind zwei Commands Teil dieses Bundles. Mit diesen kann über `vendor/bin/contao-console` sowohl ein Datenbank-Dump im Installationsordner erzeugt, als auch importiert werden.
- `vendor/bin/contao-console rapid:database:dump file.sql`
- `vendor/bin/contao-console rapid:database:import file.sql`
